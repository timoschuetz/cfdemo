# Summary
This repository contains a small fully working Proof-of-Concept on how the authentication on SAP Cloud Foundry is supposed to work. This example uses the approuter as a sidecar container, which proxies the requests to the corresponding services.
# Installation

Clone repository:

```
git clone https://gitlab.com/timoschuetz/cfdemo.git
```

Create UAA service from file (ensure that you are in the root directory):

```
cf create-service xsuaa application SERVICENAME -c xs-security.json
```

Replace SERVICENAME with a name for your service

Then you have to adjust the service name in the manifest.yml for both apps to the name you specified.
```
services:
    - SERVICENAME
```
## Deploy application
Change the application name in the manifest.yml to your preference and set the host to an unique name (this will be used for generating the link to your application). ```
```
applications:
  - name: APPNAME`
    host: APPLINK
```
Then navigate to the root directory.
```
cf push
```

# Enable logging
To enable logging you have to create a application-logs service which gathers the application logs and makes them accessible and analyzable using the kibana dashbaord. This requires that the application has already been pushed once to the Cloud Foundry instance.

## Create logging service
```
cf create-service applications-logs lite SERVICENAME
```

## Bind service to application
```
cf bind-service APPNAME SERVICENAME
```
# Example - fast deploy
```
git pull https://gitlab.com/timoschuetz/cfdemo.git
cf create-service xsuaa application cfdemo -c xs-security.json
cf push
```
