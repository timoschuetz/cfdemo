const express = require('express');
const passport = require('passport');
const xsenv = require('@sap/xsenv');
const JWTStrategy = require('@sap/xssec').JWTStrategy;
const app = express();
const log = require('cf-nodejs-logging-support');

const services = xsenv.getServices({ uaa:'cfdemo'});

log.setLoggingLevel('info');

app.use(log.logNetwork);

app.get('/health', function (req, res) {
    req.logger.info("Got health check");
    res.sendStatus(200);
});

passport.use(new JWTStrategy(services.uaa));
app.use(passport.initialize());
app.use(passport.authenticate('JWT', {session: false}));

app.get('/', function (req, res) {
    res.send('Hello ' + req.user.name.givenName + ', your ID is: ' + req.user.id + ' and your email is ' + req.user.emails[0].value + ' right?');
});

app.get('/check', function (req, res) {
    res.send('Checked!');
});

const port = process.env.PORT || 3000;
app.listen(port, function() {
    log.info('Server is listening on port %d', port);
});
